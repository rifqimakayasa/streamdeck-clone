part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const SETTINGS = _Paths.SETTINGS;
  static const CONNECT_SERVER = _Paths.CONNECT_SERVER;
  static const LIST_IMAGES = _Paths.LIST_IMAGES;
}

abstract class _Paths {
  static const HOME = '/home';
  static const SETTINGS = '/settings';
  static const CONNECT_SERVER = '/connect-server';
  static const LIST_IMAGES = '/list-images';
}
