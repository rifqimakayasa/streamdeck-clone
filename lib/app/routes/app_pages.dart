import 'package:get/get.dart';

import '../modules/connect_server/bindings/connect_server_binding.dart';
import '../modules/connect_server/views/connect_server_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/list_images/bindings/list_images_binding.dart';
import '../modules/list_images/views/list_images_view.dart';
import '../modules/settings/bindings/settings_binding.dart';
import '../modules/settings/views/settings_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.CONNECT_SERVER;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SETTINGS,
      page: () => const SettingsView(),
      binding: SettingsBinding(),
    ),
    GetPage(
      name: _Paths.CONNECT_SERVER,
      page: () => const ConnectServerView(),
      binding: ConnectServerBinding(),
    ),
    GetPage(
      name: _Paths.LIST_IMAGES,
      page: () => const ListImagesView(),
      binding: ListImagesBinding(),
    ),
  ];
}
