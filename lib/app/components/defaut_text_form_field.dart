import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../constant.dart';

class DefaultTextFormField extends StatelessWidget {
  const DefaultTextFormField({
    super.key,
    this.initialValue = '',
    this.hintText,
    this.suffixText,
    this.onChanged,
    this.inputType,
  });
  final String initialValue;
  final Function(String)? onChanged;
  final String? hintText;
  final String? suffixText;
  final TextInputType? inputType;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      // initialValue: controller.ip.value,
      initialValue: initialValue,
      style: TextStyle(
        color: kPrimaryColorAccent,
      ),
      keyboardType: inputType,
      decoration: InputDecoration(
        // hintText: 'example : 192.168.1.2',
        border: OutlineInputBorder(),
        hintText: hintText,
        hintStyle: TextStyle(
          decoration: TextDecoration.underline,
          fontStyle: FontStyle.italic,
        ),
        suffixStyle: TextStyle(
          color: kPrimaryColor,
          fontStyle: FontStyle.italic,
        ),
        suffixText: suffixText,
      ),
      // onChanged: (value) {
      //   controller.ip.value = value;
      // },
      onChanged: onChanged,
    );
  }
}
