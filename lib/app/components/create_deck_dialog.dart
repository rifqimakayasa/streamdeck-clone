import 'dart:convert';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_inset_box_shadow/flutter_inset_box_shadow.dart' as inset;
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:streamdeck/app/components/defaut_text_form_field.dart';
import 'package:streamdeck/app/constant.dart';
import 'package:streamdeck/app/routes/app_pages.dart';
import 'package:streamdeck/app/utils/function_utils.dart';

import 'default_text.dart';

class CreateDeckDialog extends StatefulWidget {
  const CreateDeckDialog({
    super.key,
    required this.title,
    // required this.isElevated,
    this.field1Name = '',
    this.field1hint,
    this.field1InitialValue,
    this.field2Name = 'Detail',
    this.field2hint,
    this.field2InitialValue,
    this.isEdit = false,
    this.image_uuid = '',
  });
  final String title;

  final String field1Name;
  final String? field1hint;
  final String? field1InitialValue;

  final String field2Name;
  final String? field2hint;
  final String? field2InitialValue;

  final String image_uuid;

  final bool isEdit;
  // final bool isElevated;

  @override
  State<CreateDeckDialog> createState() => _CreateDeckDialogState();
}

class _CreateDeckDialogState extends State<CreateDeckDialog> {
  var box = GetStorage();
  bool isElevated = true;
  bool isDeleteElevated = true;
  String? _base64;
  String? uuid;

  var data = '';
  var detail = '';

  @override
  void initState() {
    super.initState();
    if (widget.field1InitialValue != null) {
      data = widget.field1InitialValue!;
    }
    if (widget.field2InitialValue != null) {
      detail = widget.field2InitialValue!;
    }
    if (widget.isEdit && isNotEmpty(widget.image_uuid)) {
      uuid = widget.image_uuid;
      List imageData = box.read(kImages);
      var image = imageData.firstWhere(
        (element) => element['uuid'] == uuid,
        orElse: () {
          null;
        },
      );
      if (image == null) {
        uuid == '';
        return;
      }
      _base64 = image['base64'];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: kBgBlack,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: SingleChildScrollView(
        child: GestureDetector(
          onTap: () {
            Get.focusScope!.unfocus();
          },
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                tileMode: TileMode.mirror,
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  kBgBlack,
                  kBlackAccent2,
                ],
              ),
              borderRadius: BorderRadius.circular(10),
              boxShadow: kDefBoxShadow,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.center,
                  child: DefText(
                    widget.title,
                    color: kPrimaryColor,
                  ).extraLarge,
                ),
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.centerLeft,
                  child: DefText(
                    widget.field1Name,
                    color: kPrimaryColor,
                  ).semilarge,
                ),
                SizedBox(height: 5),
                Container(
                  decoration: inset.BoxDecoration(
                    boxShadow: kDefBoxShadowInset,
                  ),
                  child: DefaultTextFormField(
                    hintText: widget.field1hint,
                    initialValue: widget.field1InitialValue ?? '',
                    onChanged: (value) {
                      data = value;
                    },
                  ),
                ),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.centerLeft,
                  child: DefText(
                    widget.field2Name,
                    color: kPrimaryColor,
                  ).semilarge,
                ),
                SizedBox(height: 5),
                Container(
                  decoration: inset.BoxDecoration(
                    boxShadow: kDefBoxShadowInset,
                  ),
                  child: DefaultTextFormField(
                    hintText: widget.field2hint,
                    initialValue: widget.field2InitialValue ?? '',
                    onChanged: (value) {
                      detail = value;
                    },
                  ),
                ),
                SizedBox(height: 20),
                if (isEmpty(uuid))
                  Align(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      onTap: () async {
                        var res = await Get.toNamed(
                          Routes.LIST_IMAGES,
                        );
                        if (isNotEmpty(res)) {
                          List temp = box.read(kImages);
                          var dataImage = temp.firstWhere((element) => element['uuid'] == res);
                          _base64 = dataImage['base64'];
                          uuid = res;
                        }
                        setState(() {});
                      },
                      child: DottedBorder(
                        color: kPrimaryColor,
                        child: Container(
                          height: 150,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          // color: Colors.grey,
                          padding: EdgeInsets.all(10),
                          child: ClipRRect(
                            child: Image.asset(
                              'assets/images/default_picture.jpeg',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                if (isNotEmpty(uuid))
                  Align(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      onTap: () async {
                        var res = await Get.toNamed(
                          Routes.LIST_IMAGES,
                        );
                        if (isNotEmpty(res)) {
                          List temp = box.read(kImages);
                          var dataImage = temp.firstWhere((element) => element['uuid'] == res);
                          _base64 = dataImage['base64'];
                        }
                        setState(() {});
                      },
                      child: DottedBorder(
                        child: Container(
                          padding: EdgeInsets.all(10),
                          constraints: BoxConstraints(
                            maxHeight: 160,
                          ),
                          child: Image.memory(base64Decode(_base64!)),
                        ),
                      ),
                    ),
                  ),
                SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    GestureDetector(
                      onTapDown: (details) {
                        isElevated = false;
                        setState(() {});
                      },
                      onTapUp: (details) async {
                        await Future.delayed(kDefaultFastDuration);
                        isElevated = true;
                        setState(() {});
                        var res = {};
                        if (widget.isEdit) {
                          res = {
                            'is_delete': false,
                            'new_data': data,
                            'new_detail': detail,
                            'new_image_uuid': uuid ?? '',
                          };
                        } else {
                          res = {
                            'data': data,
                            'detail': detail,
                            'image_uuid': uuid ?? '',
                          };
                        }
                        Get.back(result: res);
                      },
                      child: AnimatedContainer(
                        duration: kDefaultFastDuration,
                        curve: kDefaultCurve,
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        height: 50,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: kBgBlack,
                          boxShadow: isElevated ? kDefBoxShadow : null,
                        ),
                        child: Center(
                          child: DefText(
                            widget.isEdit ? 'Edit' : 'Create',
                            color: kPrimaryColor,
                          ).large,
                        ),
                      ),
                    ),
                    Visibility(
                      visible: widget.isEdit,
                      child: GestureDetector(
                        onTapDown: (details) {
                          isDeleteElevated = false;
                          setState(() {});
                        },
                        onTapUp: (details) async {
                          await Future.delayed(kDefaultFastDuration);
                          isDeleteElevated = true;
                          setState(() {});
                          var res = {
                            'is_delete': true,
                          };
                          Get.back(result: res);
                        },
                        child: AnimatedContainer(
                          duration: kDefaultFastDuration,
                          curve: kDefaultCurve,
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          height: 50,
                          width: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: kBgBlack,
                            boxShadow: isDeleteElevated ? kDefBoxShadow : null,
                          ),
                          child: Center(
                            child: DefText(
                              'Delete',
                              color: kPrimaryColor,
                            ).large,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
