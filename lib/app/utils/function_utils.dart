import 'dart:convert';
import 'dart:developer' as dev;

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import '../components/default_text.dart';
import '../constant.dart';

void logKey([key, content]) {
  String finalLog = '';
  dynamic tempContent = content ?? key;
  if (tempContent is Map || tempContent is List) {
    try {
      finalLog = json.encode(tempContent);
    } catch (e) {
      finalLog = tempContent.toString();
    }
  } else if (tempContent is String) {
    finalLog = tempContent;
  } else {
    finalLog = tempContent.toString();
  }

  if (content != null) {
    dev.log('$key => $finalLog');
  } else {
    dev.log(finalLog);
  }
}

Widget loading({double? size}) {
  return Center(
    child: Column(
      mainAxisSize: MainAxisSize.min,
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: (size ?? 50) * 2,
          width: (size ?? 50) * 2,
          // color: kBgWhite,
          child: Center(
            child: SpinKitFoldingCube(
              size: size ?? 50,
              // color: kPrimaryColor,
              itemBuilder: (context, index) {
                return Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                    color: index == 0
                        ? kBlackAccent
                        : index == 1
                            ? kBlackAccent2
                            : index == 2
                                ? kBlackAccent3
                                : kBlackAccent4,
                    // boxShadow: kDefBoxShadowPrimaryColor,
                  ),
                );
              },
            ),
          ),
        ),
      ],
    ),
  );
}

void dialogLoading({double? size}) {
  Get.dialog(
    loading(size: size),
    barrierDismissible: false,
    // barrierColor: kPrimaryColor.withOpacity(0.8),
  );
}

bool isEmpty(dynamic val) {
  return [
    "",
    " ",
    '',
    ' ',
    null,
    'null',
    '{}',
    '[]',
    '0',
    '0.0',
  ].contains(val.toString());
}

bool isNotEmpty(dynamic val) {
  return ![
    "",
    " ",
    null,
    'null',
    '{}',
    '[]',
    '0',
    '0.0',
    '0.00',
  ].contains(val.toString());
}

void networkDialog({required String message}) {
  Get.dialog(
    Dialog(
      child: Container(
        height: Get.height * 0.20,
        padding: EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(
          color: kBgBlack,
          borderRadius: BorderRadius.circular(10),
          boxShadow: kDefBoxShadow,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DefText(
              message,
              textAlign: TextAlign.center,
              color: kPrimaryColor,
            ).semilarge,
          ],
        ),
      ),
      backgroundColor: kBgBlack,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
    ),
    barrierColor: kBgBlack.withOpacity(0.8),
    transitionCurve: kDefaultCurve,
    transitionDuration: kDefaultDuration,
  );
}
