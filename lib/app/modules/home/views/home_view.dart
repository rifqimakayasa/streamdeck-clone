import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:streamdeck/app/constant.dart';
import 'package:streamdeck/app/helpers/decktype.dart';
import 'package:streamdeck/app/modules/home/components/deck_item.dart';
import 'package:streamdeck/app/utils/function_utils.dart';

import '../../../components/default_text.dart';
import '../../../models/deck_item_model.dart';
import '../../../routes/app_pages.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    return Scaffold(
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        overlayColor: Colors.black,
        animationCurve: Curves.easeInOutCirc,
        animationDuration: kDefaultFastDuration,
        curve: Curves.easeInOutCirc,
        gradientBoxShape: BoxShape.circle,
        gradient: LinearGradient(
          colors: [
            kPrimaryColorAccent,
            kPrimaryColorAccent2,
          ],
        ),
        overlayOpacity: 0.6,
        children: [
          SpeedDialChild(
            label: 'Add Music Deck',
            labelBackgroundColor: kPrimaryColorAccent,
            backgroundColor: kPrimaryColor,
            child: Icon(
              Icons.music_note,
              color: kBlackAccent,
            ),
            onTap: () {
              controller.addDeckItem(type: Decktype.music.name);
            },
          ),
          SpeedDialChild(
            label: 'Add source',
            labelBackgroundColor: kPrimaryColorAccent,
            backgroundColor: kPrimaryColor,
            child: Icon(
              Icons.source_outlined,
              color: kBlackAccent2,
            ),
            onTap: () {
              controller.addDeckItem(type: Decktype.source.name);
            },
          ),
          SpeedDialChild(
            child: Icon(
              Icons.delete,
            ),
            onTap: () {
              controller.box.remove(kDeck);
              // controller.box.remove(kImages);
            },
          ),
          SpeedDialChild(
            child: Icon(
              Icons.check,
            ),
            onTap: () {
              logKey('listPage', controller.listPage);
            },
          ),
        ],
      ),
      // backgroundColor: kBgBlack,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            tileMode: TileMode.mirror,
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              kBgBlack,
              kBlackAccent2,
            ],
          ),
        ),
        height: Get.height,
        child: Column(
          children: [
            SizedBox(height: 40),
            Container(
              height: Get.height * 0.12,
              margin: EdgeInsets.symmetric(horizontal: 20),
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              decoration: BoxDecoration(
                color: kBgBlack,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: kBlackAccent,
                    offset: const Offset(4.5, 4.5),
                    blurRadius: 6.5,
                    spreadRadius: 0.2,
                  ),
                  BoxShadow(
                    color: kBlackAccent2,
                    offset: const Offset(-4.5, -4.5),
                    blurRadius: 6.5,
                    spreadRadius: 0.2,
                  ),
                ],
              ),
              child: Row(
                children: [
                  Obx(
                    () => GestureDetector(
                      onTap: () async {
                        controller.exitElevated.value = !controller.exitElevated.value;
                        await Future.delayed(kDefaultFastDuration);
                        controller.socketC.disconnectSocket();
                        controller.exitElevated.value = !controller.exitElevated.value;
                      },
                      child: AnimatedContainer(
                        duration: kDefaultFastDuration,
                        curve: kDefaultCurve,
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: kBgBlack,
                          boxShadow: controller.exitElevated.value
                              ? [
                                  BoxShadow(
                                    color: kBlackAccent,
                                    offset: const Offset(4.5, 4.5),
                                    blurRadius: 6.5,
                                    spreadRadius: 0.2,
                                  ),
                                  BoxShadow(
                                    color: kBlackAccent2,
                                    offset: const Offset(-4.5, -4.5),
                                    blurRadius: 6.5,
                                    spreadRadius: 0.2,
                                  ),
                                ]
                              : [],
                        ),
                        child: Icon(
                          Icons.exit_to_app_outlined,
                          color: kPrimaryColorAccent2,
                          size: 15,
                        ),
                      ),
                    ),
                  ),
                  Spacer(),
                  DefText(
                    'Streamdeck',
                    color: kPrimaryColor,
                  ).extraLarge,
                  Spacer(),
                  SizedBox(width: 10),
                  Obx(
                    () => GestureDetector(
                      onTap: () async {
                        controller.seettingsElevated.value = !controller.seettingsElevated.value;
                        await Future.delayed(kDefaultFastDuration);
                        Get.toNamed(Routes.SETTINGS);
                        controller.seettingsElevated.value = !controller.seettingsElevated.value;
                      },
                      child: AnimatedContainer(
                        duration: kDefaultFastDuration,
                        curve: kDefaultCurve,
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: kBgBlack,
                          boxShadow: controller.seettingsElevated.value
                              ? [
                                  BoxShadow(
                                    color: kBlackAccent,
                                    offset: const Offset(4.5, 4.5),
                                    blurRadius: 6.5,
                                    spreadRadius: 0.2,
                                  ),
                                  BoxShadow(
                                    color: kBlackAccent2,
                                    offset: const Offset(-4.5, -4.5),
                                    blurRadius: 6.5,
                                    spreadRadius: 0.2,
                                  ),
                                ]
                              : [],
                        ),
                        child: Icon(
                          Icons.settings,
                          color: kPrimaryColorAccent2,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 5),
            Obx(
              () {
                if (controller.listPage.isEmpty) {
                  return Container();
                }
                return DotsIndicator(
                  // dotsCount: controller.listPage.length == 0 ? 1 : controller.listPage.length,
                  dotsCount: controller.listPage.length,
                  position: controller.pagePosition.value,
                );
              },
            ),
            // Obx(
            //   () => NeumorphicProgress(
            //     percent: controller.progess.value,
            //     duration: kDefaultDuration,
            //     curve: kDefaultCurve,
            //     style: ProgressStyle(
            //       accent: kBgBlack,
            //       variant: kBlackAccent2,
            //       border: NeumorphicBorder(
            //         color: kBlackAccent3,
            //       ),
            //       depth: 1,
            //     ),
            //   ),
            // ),
            SizedBox(height: 5),
            Expanded(
              child: Obx(
                () => PageView.builder(
                  itemCount: controller.listPage.length,
                  controller: controller.pageController,
                  onPageChanged: (value) {
                    // controller.pageController.
                  },
                  physics: AlwaysScrollableScrollPhysics(
                    parent: BouncingScrollPhysics(),
                  ),
                  itemBuilder: (context, idx) {
                    return GridView.builder(
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                      // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      //   maxCrossAxisExtent: 100,
                      //   crossAxisSpacing: 15,
                      //   mainAxisSpacing: 15,
                      //   childAspectRatio: 1,
                      // ),
                      controller: controller.scrollController,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        // crossAxisCount: (maxItemPerPage / 2).ceil(),
                        crossAxisCount: (controller.socketC.maxItemPerPage.value / 2).ceil(),
                        crossAxisSpacing: 15,
                        mainAxisSpacing: 15,
                        mainAxisExtent: 80,
                        // childAspectRatio: 1,
                      ),
                      physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
                      // shrinkWrap: true,
                      itemCount: controller.listPage[idx].length,
                      itemBuilder: (context, index) {
                        return Obx(() {
                          // var dataDeck = DeckItemModel.fromJson(controller.listGrid[index]).obs;
                          var dataDeck = DeckItemModel.fromJson(controller.listPage[idx][index]).obs;
                          return DeckItem(
                            pageIndex: idx,
                            gridIndex: index,
                            data: dataDeck.value,
                          );
                        });
                      },
                    );
                  },
                  // itemBuilder: (context, idx) {
                  //   List a = controller.listPage[idx];
                  //   return Obx(
                  //     () => GridView.count(
                  //       crossAxisSpacing: 15,
                  //       mainAxisSpacing: 15,
                  //       crossAxisCount: 6,
                  //       physics: NeverScrollableScrollPhysics(),
                  //       padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                  //       shrinkWrap: true,
                  //       children: a
                  //           .asMap()
                  //           .map(
                  //             (key, value) {
                  //               var dataDeck = DeckItemModel.fromJson(controller.listPage[idx][key]).obs;
                  //               return MapEntry(
                  //                 key,
                  //                 DeckItem(
                  //                   data: dataDeck.value,
                  //                   index: key,
                  //                 ),
                  //               );
                  //             },
                  //           )
                  //           .values
                  //           .toList(),
                  //     ),
                  //   );
                  // },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
