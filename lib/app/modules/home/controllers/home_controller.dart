import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:streamdeck/app/components/create_deck_dialog.dart';
import 'package:streamdeck/app/controllers/socket_controller.dart';
import 'package:streamdeck/app/helpers/decktype.dart';
import 'package:streamdeck/app/models/deck_item_model.dart';
import 'package:streamdeck/app/utils/function_utils.dart';

import '../../../constant.dart';

class HomeController extends GetxController with GetSingleTickerProviderStateMixin {
  var box = GetStorage();
  var socketC = Get.find<SocketController>();
  var zzzz = 6.obs;
  var progess = 0.0.obs;
  var pagePosition = 0.0.obs;
  var addDeckButtonElevated = true.obs;

  PageController pageController = PageController();
  ScrollController scrollController = ScrollController();

  var selectedIndex = (0 - 1).obs;

  var exitElevated = true.obs;
  var seettingsElevated = true.obs;

  var listGrid = [].obs;
  var listPage = [].obs;

  var listScenes = [].obs;

  var currentSceneName = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    initialFunction();
    // progess.value = 0 / listPage.length;
    // progess.value = 0.33;
  }

  @override
  void onClose() {
    pageController.removeListener(() {});
    pageController.dispose();
  }

  // void addItem() {
  //   var item
  // }

  // void tapButton({required int index, required Function() onTap}) async {
  //   selectedIndex.value = index;
  //   await Future.delayed(kDefaultFastDuration);
  //   onTap();
  //   selectedIndex.value = -1;
  // }

  void tapButton({required int index, required Function() onTap}) async {
    await Future.delayed(kDefaultFastDuration);
    onTap();
    selectedIndex.value = -1;
  }

  void pressButton({required int index, required Function() onTap}) {}

  Future<void> initialFunction() async {
    pageController.addListener(() {
      selectedIndex.value = -1;
      pagePosition.value = pageController.page!;
      progess.value = (pageController.page! + 1) / listPage.length;
    });
    scrollController.addListener(() {
      selectedIndex.value = -1;
    });
    // dialogLoading();
    // var temp = box.read(kDeck) ?? [];
    // listPage.add(temp);
    // listPage.add(temp);
    // listPage.refresh();
    // listGrid.addAll(temp);
    // await connectSocket(ip: ip, port: port);
    // Get.back();
  }

  // void hideSource({required currentFunction, required bool isActivate, required additinalData}) {
  //   if (currentFunction == Functionality.hideSource) {
  //     if (!isActivate) {
  //       socket.write('s_hide$additinalData');
  //     } else {
  //       socket.write('s_unhide$additinalData');
  //     }
  //   }
  // }

  void addGridItem(item) {
    var index = listPage.length - 1;
    List temp = box.read(kDeck) ?? [];
    temp.add(item);
    box.write(kDeck, temp);
    var lastGridIndex = listPage.last.length;
    logKey('addGridItem', lastGridIndex);
    if (lastGridIndex >= socketC.maxItemPerPage.value) {
      var _temp = [];
      _temp.add(item);
      listPage.add(_temp);
    } else {
      listPage.last.add(item);
    }
    listPage.refresh();
  }

  void addDeckItem({required String type, dynamic data, String detail = ''}) async {
    if (type == Decktype.music.name) {
      var res = await Get.dialog(
        barrierColor: kBgBlack.withOpacity(0.8),
        transitionCurve: kDefaultCurve,
        transitionDuration: kDefaultDuration,
        CreateDeckDialog(
          title: 'Add Music Deck',
          field1Name: 'Path file suara',
          field1hint: r'Example : D:\Music\acumalaka.mp3',
          field2hint: 'Example: Acumalaka :)',
        ),
      );
      if (isEmpty(res) || isEmpty(res['data'])) {
        return;
      }
      var item = DeckItemModel(
        msg: 'playsound',
        data: res['data'],
        type: Decktype.music.name,
        detail: res['detail'],
        image_uuid: res['image_uuid'],
      ).toJson();
      addGridItem(item);
    } else if (type == Decktype.source.name) {
      var res = await Get.dialog(
        CreateDeckDialog(
          title: 'Add Source Deck',
          field1Name: 'Nama source (Must be same as on the OBS)',
          field1hint: 'Example: Webcam',
          field2hint: 'Example: Webcam Logitech',
        ),
      );
      if (isEmpty(res) || isEmpty(res['data'])) {
        return;
      }
      var item = DeckItemModel(
        msg: 's_hide',
        data: res['data'],
        type: type,
        detail: res['detail'],
        image_uuid: res['image_uuid'],
      ).toJson();
      addGridItem(item);
    }
  }

  void editDeck(
      {required int index_page, required int index_grid, required String new_data, required String new_detail, required String new_image_uuid}) async {
    selectedIndex.value = -1;
    Map newData = DeckItemModel(
      msg: listPage[index_page][index_grid]['msg'],
      data: new_data,
      detail: new_detail,
      type: listPage[index_page][index_grid]['type'],
      image_uuid: new_image_uuid,
    ).toJson();
    List temp = box.read(kDeck);
    var compareItem = listPage[index_page][index_grid];
    var index = -1;
    for (var i = 0; i < temp.length; i++) {
      var isEquals = mapEquals(temp[i], compareItem);
      index = i;
      if (isEquals) {
        break;
      }
    }
    listPage[index_page][index_grid]['data'] = new_data;
    listPage[index_page][index_grid]['detail'] = new_detail;
    listPage[index_page][index_grid]['image_uuid'] = new_image_uuid;
    temp.removeAt(index);
    temp.insert(index, newData);
    logKey('new temp', temp);
    await box.write(kDeck, temp);
    listPage.refresh();
  }

  void deleteDeck({required int index_page, required int index_grid}) async {
    selectedIndex.value = -1;
    List temp = box.read(kDeck);
    var deckItem = listPage[index_page][index_grid];
    var index = -1;
    for (var i = 0; i < temp.length; i++) {
      var isEquals = mapEquals(temp[i], deckItem);
      index = i;
      if (isEquals) {
        break;
      }
    }
    temp.removeAt(index);
    await box.write(kDeck, temp);
    listPage[index_page].removeAt(index_grid);
    listPage.refresh();
  }
}
