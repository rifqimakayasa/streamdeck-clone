import 'dart:convert';

import 'package:flutter/material.dart' hide BoxDecoration, BoxShadow;
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_inset_box_shadow/flutter_inset_box_shadow.dart';
import 'package:streamdeck/app/constant.dart';

import '../../../components/default_text.dart';
import '../../../utils/function_utils.dart';

class NeumorphicDeck extends StatelessWidget {
  const NeumorphicDeck({
    super.key,
    required this.title,
    required this.isElevated,
    this.useInset = false,
    this.subTitle = '',
    this.color = Colors.grey,
    this.imagebas64 = '',
  });
  final String title;
  final String subTitle;
  final bool isElevated;
  final MaterialColor? color;
  final bool useInset;
  final String imagebas64;

  @override
  Widget build(BuildContext context) {
    // logKey('imagebas64', imagebas64);
    return AnimatedContainer(
      duration: kDefaultFastDuration,
      padding: EdgeInsets.all(5),
      height: 100,
      width: 100,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            if (isNotEmpty(imagebas64))
              Container(
                height: 35,
                child: Image.memory(
                  base64Decode(imagebas64),
                  fit: BoxFit.cover,
                ),
              ),
            if (isEmpty(imagebas64))
              Container(
                height: 35,
                child: Image.network(
                  'https://icon-library.com/images/obs-studio-icon/obs-studio-icon-27.jpg',
                  fit: BoxFit.cover,
                ),
              ),
            SizedBox(height: 5),
            Expanded(
              child: DefText(
                '$title',
                maxLine: 2,
                color: kPrimaryColorAccent2,
                textAlign: TextAlign.center,
              ).normal,
            ),
          ],
        ),
      ),
      decoration: BoxDecoration(
        // color: kBgBlack,
        gradient: LinearGradient(
          tileMode: TileMode.mirror,
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            kBlackGradien,
            kBlackGradien2,
          ],
        ),
        borderRadius: BorderRadius.circular(10),
        boxShadow: isElevated
            ? useInset
                ? [
                    BoxShadow(
                      color: kBlackAccent3,
                      offset: Offset(4, 4),
                      blurRadius: 4.5,
                      spreadRadius: 0.2,
                      inset: true,
                    ),
                    BoxShadow(
                      color: kBlackAccent4,
                      offset: Offset(-4, -4),
                      blurRadius: 4.5,
                      spreadRadius: 0.2,
                      inset: true,
                    ),
                  ]
                : []
            : [
                // BoxShadow(
                //   color: kBlackAccent4,
                //   offset: Offset(4.5, 4.5),
                //   blurRadius: 1,
                //   spreadRadius: 0.2,
                //   inset: true,
                // ),
                // BoxShadow(
                //   color: kBlackAccent3,
                //   offset: Offset(-4.5, -4.5),
                //   blurRadius: 1,
                //   spreadRadius: 0.2,
                //   inset: true,
                // ),

                // BoxShadow(
                //   color: kBlackAccent3,
                //   offset: const Offset(4.5, 4.5),
                //   blurRadius: 6.5,
                //   spreadRadius: 0.2,
                // ),
                // BoxShadow(
                //   color: kBlackAccent4,
                //   offset: const Offset(-4.5, -4.5),
                //   blurRadius: 6.5,
                //   spreadRadius: 0.2,
                // ),

                BoxShadow(
                  color: kBlackAccent,
                  offset: const Offset(4.5, 4.5),
                  blurRadius: 6.5,
                  spreadRadius: 0.2,
                ),
                BoxShadow(
                  color: kBlackAccent2,
                  offset: const Offset(-4.5, -4.5),
                  blurRadius: 6.5,
                  spreadRadius: 0.2,
                ),

                //* Efek cekung
                // BoxShadow(
                //   color: kBlackAccent,
                //   offset: Offset(1, 1),
                //   blurRadius: 6.5,
                //   spreadRadius: 0.2,
                //   inset: true,
                // ),
                // BoxShadow(
                //   color: kBlackAccent2,
                //   offset: Offset(-1, -1),
                //   blurRadius: 6.5,
                //   spreadRadius: 0.2,
                //   inset: true,
                // ),
              ],
      ),
    );
  }
}
