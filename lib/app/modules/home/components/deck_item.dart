import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:get/get.dart';
import 'package:streamdeck/app/components/create_deck_dialog.dart';
import 'package:streamdeck/app/helpers/decktype.dart';
import 'package:streamdeck/app/models/deck_item_model.dart';
import 'package:streamdeck/app/modules/home/components/neumorphic_deck.dart';
import 'package:streamdeck/app/modules/home/controllers/home_controller.dart';

import '../../../constant.dart';
import '../../../utils/function_utils.dart';

class DeckItem extends GetView<HomeController> {
  const DeckItem({
    super.key,
    required this.data,
    required this.pageIndex,
    required this.gridIndex,
  });
  final DeckItemModel data;
  final int pageIndex;
  final int gridIndex;

  @override
  Widget build(BuildContext context) {
    if (data.type == Decktype.scene.name) {
      //? isi data untuk tipe scene adalah nama Scene
      return Obx(
        () => GestureDetector(
          onTap: () {
            controller.currentSceneName.value = data.data;
            controller.tapButton(
              index: gridIndex,
              onTap: () {
                controller.socketC.switchScene(sceneName: data.data);
              },
            );
          },
          child: NeumorphicDeck(
            useInset: true,
            title: data.data,
            isElevated: controller.currentSceneName.value == data.data,
            // color: controller.currentSceneName.value == data.data ? Colors.lightGreen : Colors.grey,
            color: Colors.green,
          ),
        ),
      );
    } else if (data.type == Decktype.music.name) {
      //? isi data untuk tipe music adalah path
      return Obx(() {
        List images = controller.box.read(kImages) ?? [];
        var _imagebase64 = '';
        if (images.isNotEmpty && isNotEmpty(data.image_uuid)) {
          var image = images.firstWhere((element) => element['uuid'] == data.image_uuid);
          _imagebase64 = image['base64'];
        }
        return GestureDetector(
          onTapDown: (details) {
            controller.selectedIndex.value = gridIndex;
          },
          onTapUp: (details) async {
            controller.tapButton(
              index: gridIndex,
              onTap: () {
                controller.socketC.playSound(data.data);
              },
            );
          },
          onLongPress: () async {
            //* Edit Deck
            controller.selectedIndex.value = -1;
            var resEdit = await Get.dialog(
              barrierColor: kBgBlack.withOpacity(0.8),
              transitionCurve: kDefaultCurve,
              transitionDuration: kDefaultDuration,
              CreateDeckDialog(
                title: 'Edit Music Deck',
                field1Name: 'Path file suara',
                field1InitialValue: data.data,
                field2hint: r'Example : D:\Music\acumalaka.mp3',
                field2InitialValue: data.detail,
                isEdit: true,
                image_uuid: data.image_uuid,
              ),
            );
            if (isEmpty(resEdit)) {
              return;
            }
            if (resEdit['is_delete']) {
              controller.deleteDeck(
                index_page: pageIndex,
                index_grid: gridIndex,
              );
            } else {
              controller.editDeck(
                index_page: pageIndex,
                index_grid: gridIndex,
                new_data: resEdit['new_data'],
                new_detail: resEdit['new_detail'],
                new_image_uuid: resEdit['new_image_uuid'],
              );
            }
          },
          onLongPressEnd: (details) async {
            await Future.delayed(kDefaultFastDuration);
            controller.selectedIndex.value = -1;
          },
          onLongPressUp: () {
            logKey('onLongPressUp');
            controller.selectedIndex.value = -1;
          },
          child: NeumorphicDeck(
            title: data.detail,
            isElevated: controller.selectedIndex.value == gridIndex,
            imagebas64: _imagebase64,
            color: Colors.lightBlue,
          ),
        );
      });
    } else if (data.type == Decktype.source.name) {
      //? isi data untuk tipe source adalah nama source nya
      return Obx(
        () => GestureDetector(
          onTapDown: (details) {
            controller.selectedIndex.value = gridIndex;
          },
          onTapUp: (details) async {
            controller.tapButton(
              index: gridIndex,
              onTap: () {
                controller.socketC.hideSource(sourceName: data.data);
              },
            );
          },
          onLongPress: () async {
            //* Edit Deck
            controller.selectedIndex.value = -1;
            var resEdit = await Get.dialog(
              barrierColor: kBgBlack.withOpacity(0.8),
              transitionCurve: kDefaultCurve,
              transitionDuration: kDefaultDuration,
              CreateDeckDialog(
                title: 'Edit Music Deck',
                field1Name: 'Path file suara',
                field1InitialValue: data.data,
                field2hint: r'Example : D:\Music\acumalaka.mp3',
                field2InitialValue: data.detail,
                isEdit: true,
                image_uuid: data.image_uuid,
              ),
            );
            if (isEmpty(resEdit)) {
              return;
            }
            if (resEdit['is_delete']) {
              controller.deleteDeck(
                index_page: pageIndex,
                index_grid: gridIndex,
              );
            } else {
              controller.editDeck(
                index_page: pageIndex,
                index_grid: gridIndex,
                new_data: resEdit['new_data'],
                new_detail: resEdit['new_detail'],
                new_image_uuid: resEdit['new_image_uuid'],
              );
            }
          },
          onLongPressEnd: (details) async {
            await Future.delayed(kDefaultFastDuration);
            controller.selectedIndex.value = -1;
          },
          onLongPressUp: () {
            logKey('onLongPressUp');
            controller.selectedIndex.value = -1;
          },
          child: NeumorphicDeck(
            title: data.detail,
            isElevated: controller.selectedIndex.value == gridIndex,
            color: Colors.brown,
          ),
        ),
      );
    }
    return Container();
  }
}
