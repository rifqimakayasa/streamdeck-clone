import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inset_box_shadow/flutter_inset_box_shadow.dart' as inset;
import 'package:get/get.dart';
import 'package:streamdeck/app/components/default_text.dart';
import 'package:streamdeck/app/routes/app_pages.dart';

import '../../../constant.dart';
import '../controllers/settings_controller.dart';

class SettingsView extends GetView<SettingsController> {
  const SettingsView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('SettingsView'),
      //   centerTitle: true,
      // ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controller.getAudioDevices();
        },
      ),
      body: Container(
        height: Get.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            tileMode: TileMode.mirror,
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              kBgBlack,
              kBlackAccent2,
            ],
          ),
        ),
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 40),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                width: Get.width,
                height: Get.height * 0.12,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: kBgBlack,
                  boxShadow: kDefBoxShadow,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Obx(
                      () => GestureDetector(
                        onTap: () async {
                          controller.backElevated.value = !controller.backElevated.value;
                          await Future.delayed(kDefaultFastDuration);
                          controller.backElevated.value = !controller.backElevated.value;
                          var rawData = controller.box.read('raw');
                          controller.socketC.arrangeItem(rawData, resetPageController: true);
                          Get.back();
                        },
                        child: AnimatedContainer(
                          duration: kDefaultFastDuration,
                          curve: kDefaultCurve,
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: kBgBlack,
                            boxShadow: controller.backElevated.value
                                ? [
                                    BoxShadow(
                                      color: kBlackAccent,
                                      offset: const Offset(4.5, 4.5),
                                      blurRadius: 6.5,
                                      spreadRadius: 0.2,
                                    ),
                                    BoxShadow(
                                      color: kBlackAccent2,
                                      offset: const Offset(-4.5, -4.5),
                                      blurRadius: 6.5,
                                      spreadRadius: 0.2,
                                    ),
                                  ]
                                : [],
                          ),
                          child: Icon(
                            Icons.arrow_back_ios_new_outlined,
                            color: kPrimaryColorAccent2,
                            size: 15,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Expanded(
                      child: DefText(
                        'Settings',
                        color: kPrimaryColor,
                      ).extraLarge,
                    ),
                    Spacer(),
                  ],
                ),
              ),
              SizedBox(height: 15),
              DefText(
                'Audio Device for Music',
                color: kPrimaryColor,
              ).large,
              SizedBox(height: 10),
              Obx(
                () => Container(
                  constraints: BoxConstraints(
                    maxWidth: Get.width / 2,
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton2(
                      isExpanded: true,
                      style: TextStyle(
                        color: kPrimaryColor,
                      ),
                      dropdownMaxHeight: 200,
                      dropdownPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      iconEnabledColor: kPrimaryColor,
                      buttonPadding: EdgeInsets.only(right: 15),
                      buttonDecoration: inset.BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: kBlackGradien2,
                        boxShadow: kDefBoxShadowInset,
                        border: Border.all(
                          color: Colors.transparent,
                        ),
                      ),
                      dropdownDecoration: inset.BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: kBlackGradien2,
                        boxShadow: kDefBoxShadowInset,
                      ),
                      value: controller.selectedAudioDevice.value,
                      icon: const Icon(
                        Icons.arrow_forward_ios_outlined,
                      ),
                      iconSize: 14,
                      items: controller.listAudioDevices
                          .map(
                            (device) => DropdownMenuItem<String>(
                              value: device,
                              child: DefText(device, color: kPrimaryColor).normal,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        controller.selectAudioDevice(value!);
                      },
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              DefText('Item per page', color: kPrimaryColor).large,
              SizedBox(height: 5),
              Obx(
                () => DefText(
                  '${controller.sliderValue.value}',
                  color: kPrimaryColor,
                ).normal,
              ),
              Obx(
                () => Slider(
                  value: controller.sliderValue.value.roundToDouble(),
                  min: 4,
                  max: 26,
                  onChanged: (value) {
                    // controller.sliderValue.value = value.round();
                    controller.slide(value.round());
                  },
                ),
              ),
              SizedBox(height: 10),
              DefText('Images', color: kPrimaryColor).large,
              SizedBox(height: 10),
              GestureDetector(
                onTap: () {
                  Get.toNamed(Routes.LIST_IMAGES);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  decoration: BoxDecoration(
                    color: kBgBlack,
                    boxShadow: kDefBoxShadow,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: DefText('List Images', color: kPrimaryColor).large,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
