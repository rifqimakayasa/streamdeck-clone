import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:streamdeck/app/constant.dart';
import 'package:streamdeck/app/controllers/socket_controller.dart';
import 'package:streamdeck/app/modules/home/controllers/home_controller.dart';

class SettingsController extends GetxController {
  var box = GetStorage();
  var isLoading = true.obs;
  var homeC = Get.find<HomeController>();
  var socketC = Get.find<SocketController>();
  var listAudioDevices = [].obs;
  var selectedAudioDevice = 'Default'.obs;
  final count = 0.obs;

  var backElevated = true.obs;

  var sliderValue = 0.obs;

  void slide(value) {
    sliderValue.value = value;
    if (value % 2 == 0) {
      socketC.maxItemPerPage.value = value;
    } else {
      socketC.maxItemPerPage.value = value + 1;
    }
  }

  @override
  void onInit() {
    super.onInit();
    initialFunction();
    sliderValue.value = socketC.maxItemPerPage.value;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void selectAudioDevice(String value) async {
    selectedAudioDevice.value = value;
    await box.write(kAudioDevice, selectedAudioDevice.value);
    socketC.changeAudioDevice(selectedAudioDevice.value);
  }

  void initialFunction() async {
    // homeC.socket.listen((event) {
    //   logKey('socket listen settings');
    // });
    selectedAudioDevice.value = box.read(kAudioDevice);
    listAudioDevices.add(selectedAudioDevice.value);
    getAudioDevices();
  }

  void getAudioDevices() {
    if (socketC.connected.value) {
      socketC.socket.write('audiodevices');
    }
  }

  void increment() => count.value++;
}
