import 'package:flutter/material.dart';
import 'package:flutter_inset_box_shadow/flutter_inset_box_shadow.dart' as inset;
import 'package:get/get.dart';
import 'package:streamdeck/app/components/default_text.dart';
import 'package:streamdeck/app/components/defaut_text_form_field.dart';
import 'package:streamdeck/app/constant.dart';

import '../controllers/connect_server_controller.dart';

class ConnectServerView extends GetView<ConnectServerController> {
  const ConnectServerView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Get.put(ConnectServerController());
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('ConnectServerView'),
      //   centerTitle: true,
      // ),
      // floatingActionButton: FloatingActionButton(
      // onPressed: () {
      // controller.tez();
      // Get.focusScope!.unfocus();
      // },
      // ),
      backgroundColor: kBgBlack,
      body: SingleChildScrollView(
        // padding: EdgeInsets.symmetric(horizontal: 20),
        child: GestureDetector(
          onTap: () {
            Get.focusScope!.unfocus();
          },
          child: Container(
            height: Get.height,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                tileMode: TileMode.mirror,
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  kBgBlack,
                  kBlackAccent2,
                ],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 40),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  width: Get.width,
                  height: Get.height * 0.12,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: kBgBlack,
                    boxShadow: kDefBoxShadow,
                  ),
                  child: Center(
                    child: DefText(
                      'CONNECT',
                      color: kPrimaryColor,
                    ).large,
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: DefText(
                    'IP Server',
                    color: kPrimaryColor,
                  ).large,
                ),
                SizedBox(height: 5),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  decoration: inset.BoxDecoration(
                    boxShadow: kDefBoxShadowInset,
                  ),
                  child: DefaultTextFormField(
                    initialValue: controller.ip.value,
                    inputType: TextInputType.number,
                    onChanged: (value) {
                      controller.ip.value = value;
                    },
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: DefText(
                    'Port',
                    color: kPrimaryColor,
                  ).large,
                ),
                SizedBox(height: 5),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  decoration: inset.BoxDecoration(
                    boxShadow: kDefBoxShadowInset,
                  ),
                  child: Obx(
                    // () => TextFormField(
                    //   initialValue: '6969',
                    //   keyboardType: TextInputType.number,
                    //   style: TextStyle(
                    //     color: kPrimaryColorAccent,
                    //   ),
                    //   decoration: InputDecoration(
                    //     hintText: 'example : 6969',
                    //     suffixText: controller.port.value == '6969' ? 'Nice! ' : null,
                    //     border: OutlineInputBorder(),
                    //     hintStyle: TextStyle(
                    //       decoration: TextDecoration.underline,
                    //       fontStyle: FontStyle.italic,
                    //     ),
                    //     suffixStyle: TextStyle(
                    //       color: kPrimaryColor,
                    //       fontStyle: FontStyle.italic,
                    //     ),
                    //   ),
                    //   onChanged: (value) {
                    //     controller.port.value = value;
                    //   },
                    // ),
                    () => DefaultTextFormField(
                      initialValue: controller.port.value,
                      inputType: TextInputType.number,
                      suffixText: controller.port.value == '6969' ? 'Nice! ' : null,
                      onChanged: (value) {
                        controller.ip.value = value;
                      },
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Center(
                  child: GestureDetector(
                    onTap: () async {
                      controller.isElevated.value = !controller.isElevated.value;
                      await Future.delayed(kDefaultFastDuration);
                      controller.isElevated.value = !controller.isElevated.value;
                      controller.connect();
                    },
                    // onPanDown: (details) {
                    //   controller.isElevated.value = !controller.isElevated.value;
                    // },
                    // onPanCancel: () {
                    //   controller.isElevated.value = !controller.isElevated.value;
                    // },

                    child: Obx(
                      () => AnimatedContainer(
                        duration: kDefaultFastDuration,
                        curve: kDefaultCurve,
                        width: 200,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: controller.isElevated.value ? kDefBoxShadow : null,
                          color: kBgBlack,
                        ),
                        padding: EdgeInsets.all(10),
                        child: Center(
                          child: DefText(
                            'connect',
                            color: kPrimaryColor,
                          ).semilarge,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
