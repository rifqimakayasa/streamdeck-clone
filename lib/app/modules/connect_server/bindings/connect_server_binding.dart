import 'package:get/get.dart';

import '../controllers/connect_server_controller.dart';

class ConnectServerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ConnectServerController>(
      () => ConnectServerController(),
    );
  }
}
