import 'package:get/get.dart';
import 'package:streamdeck/app/controllers/socket_controller.dart';
import 'package:streamdeck/app/utils/function_utils.dart';

class ConnectServerController extends GetxController {
  var socketC = Get.find<SocketController>();
  // var ip = '10.0.2.2'.obs;
  var ip = '192.168.0.2'.obs;
  var port = '6969'.obs;
  var isElevated = true.obs;

  void tez() {
    // List tes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27];
    List tes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
    List listPage = [];
    var length = tes.length / 12;
    var ceil = length.ceil();
    for (var i = 0; i < ceil; i++) {
      var indexCeil = (i + 1) * 12;
      List temp = [];
      for (var x = indexCeil - 11; x <= indexCeil; x++) {
        // var idx = x - 1;
        logKey('xx', x);
        if (x > tes.length) {
          break;
        }
        // logKey('ini x looping ke ${i + 1}', x);
        // logKey('isi tes', tes[x - 1]);
        temp.add(tes[x - 1]);
      }
      listPage.add(temp);
    }
    logKey('tes', tes);
    logKey('listPage', listPage);
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (isNotEmpty(Get.arguments)) {
      if (Get.arguments['disconnected']) {
        networkDialog(message: "You've been disconnected");
      }
    }
  }

  @override
  void onClose() {
    super.onClose();
  }

  void connect() async {
    if (isEmpty(ip.value) || isEmpty(port.value)) {
      return;
    }
    await socketC.connectSocket(
      ip: ip.value,
      port: port.value,
    );
    if (socketC.connected.value) {
      socketC.getScenes();
      socketC.cronScene();
    }
  }
}
