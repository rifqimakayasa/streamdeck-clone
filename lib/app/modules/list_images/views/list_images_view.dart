import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/default_text.dart';
import '../../../constant.dart';
import '../controllers/list_images_controller.dart';

class ListImagesView extends GetView<ListImagesController> {
  const ListImagesView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Get.put(ListImagesController());
    return Scaffold(
      body: Container(
        decoration: kDefaultBackgroundDecoration,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 40),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              width: Get.width,
              height: Get.height * 0.12,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: kBgBlack,
                boxShadow: kDefBoxShadow,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Obx(
                    () => GestureDetector(
                      onTap: () async {
                        controller.backElevated.value = !controller.backElevated.value;
                        await Future.delayed(kDefaultFastDuration);
                        controller.backElevated.value = !controller.backElevated.value;
                        await Future.delayed(kDefaultFastDuration);
                        // var rawData = controller.box.read('raw');
                        // controller.socketC.arrangeItem(rawData, resetPageController: true);
                        Get.back();
                      },
                      child: AnimatedContainer(
                        duration: kDefaultFastDuration,
                        curve: kDefaultCurve,
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: kBgBlack,
                          boxShadow: controller.backElevated.value
                              ? [
                                  BoxShadow(
                                    color: kBlackAccent,
                                    offset: const Offset(4.5, 4.5),
                                    blurRadius: 6.5,
                                    spreadRadius: 0.2,
                                  ),
                                  BoxShadow(
                                    color: kBlackAccent2,
                                    offset: const Offset(-4.5, -4.5),
                                    blurRadius: 6.5,
                                    spreadRadius: 0.2,
                                  ),
                                ]
                              : [],
                        ),
                        child: Icon(
                          Icons.arrow_back_ios_new_outlined,
                          color: kPrimaryColorAccent2,
                          size: 15,
                        ),
                      ),
                    ),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Expanded(
                    child: DefText(
                      'List Images',
                      color: kPrimaryColor,
                    ).extraLarge,
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Obx(
                    () => GestureDetector(
                      onTap: () async {
                        controller.addElevated.value = !controller.addElevated.value;
                        await Future.delayed(kDefaultFastDuration);
                        controller.addElevated.value = !controller.addElevated.value;
                        await Future.delayed(kDefaultFastDuration);
                        controller.importImages();
                      },
                      child: AnimatedContainer(
                        duration: kDefaultFastDuration,
                        curve: kDefaultCurve,
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: kBgBlack,
                          boxShadow: controller.addElevated.value
                              ? [
                                  BoxShadow(
                                    color: kBlackAccent,
                                    offset: const Offset(4.5, 4.5),
                                    blurRadius: 6.5,
                                    spreadRadius: 0.2,
                                  ),
                                  BoxShadow(
                                    color: kBlackAccent2,
                                    offset: const Offset(-4.5, -4.5),
                                    blurRadius: 6.5,
                                    spreadRadius: 0.2,
                                  ),
                                ]
                              : [],
                        ),
                        child: Icon(
                          Icons.add_outlined,
                          color: kPrimaryColorAccent2,
                          size: 15,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Expanded(
              child: Container(
                child: Obx(
                  () => GridView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 100,
                      mainAxisExtent: 100,
                      crossAxisSpacing: 15,
                      mainAxisSpacing: 15,
                      childAspectRatio: 1,
                    ),
                    shrinkWrap: true,
                    itemCount: controller.images.length,
                    itemBuilder: (context, index) {
                      var _byte = base64Decode(
                        controller.images[index]['base64'],
                      );
                      var _uuid = controller.images[index]['uuid'];
                      var isElevated = true.obs;
                      return Obx(
                        () => GestureDetector(
                          onTap: () async {
                            if (controller.isTapped.value) {
                              return;
                            }
                            controller.isTapped.value = true;
                            isElevated.value = !isElevated.value;
                            await Future.delayed(kDefaultFastDuration);
                            controller.isTapped.value = false;
                            Get.back(result: _uuid);
                          },
                          // onTapDown: (details) {
                          //   isElevated.value = false;
                          // },
                          // onTapUp: (details) {
                          //   isElevated.value = true;
                          // },
                          child: AnimatedContainer(
                            duration: kDefaultFastDuration,
                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: kBgBlack,
                              boxShadow: isElevated.value ? kDefBoxShadow : null,
                            ),
                            child: Image.memory(
                              _byte,
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
