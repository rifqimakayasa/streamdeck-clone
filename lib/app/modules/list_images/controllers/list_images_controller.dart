import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image/image.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_size_getter/file_input.dart';
import 'package:image_size_getter/image_size_getter.dart';
import 'package:streamdeck/app/constant.dart';
import 'package:streamdeck/app/utils/function_utils.dart';
import 'package:uuid/uuid.dart';

class ListImagesController extends GetxController {
  var uuid = Uuid();
  var box = GetStorage();
  final imagePicker = ImagePicker();
  var backElevated = true.obs;
  var addElevated = true.obs;
  var isTapped = false.obs;

  var images = [].obs;

  @override
  void onInit() {
    super.onInit();
    initialFunction();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void initialFunction() {
    var _temp = box.read(kImages) ?? [];
    images.addAll(_temp);
    logKey('images length', images.length);
  }

  void importImages() async {
    var xIMages = await imagePicker.pickMultiImage();
    var list = box.read(kImages) ?? [];
    if (xIMages.isEmpty) {
      return;
    }
    for (var xImage in xIMages) {
      var _uuid = uuid.v4();
      var file = File(xImage.path);
      var size = ImageSizeGetter.getSize(FileInput(file));
      var height = size.height;
      var width = size.width;
      var bytes = await xImage.readAsBytes();
      var image = decodeImage(bytes);
      var resizedImage = copyResize(image!, height: height, width: width);
      var _base64 = base64Encode(encodePng(resizedImage));
      list.add(
        {'uuid': _uuid, 'base64': _base64},
      );
    }
    images.clear();
    images.addAll(list);
    box.write(kImages, list);
  }
}
