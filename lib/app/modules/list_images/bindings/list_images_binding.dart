import 'package:get/get.dart';

import '../controllers/list_images_controller.dart';

class ListImagesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ListImagesController>(
      () => ListImagesController(),
    );
  }
}
