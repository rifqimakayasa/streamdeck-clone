import 'package:get/get.dart';
import 'package:streamdeck/app/controllers/socket_controller.dart';

class GlobalBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(SocketController());
  }
}
