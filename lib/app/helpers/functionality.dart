enum Functionality {
  getScenes,
  muteAudio,
  switchScene,
  hideSource,
  toggleStream,
  toggleRecording,
  pauseRecording,
  disconnect,
  filename,
  getCurrentScene,
  audiodevices,
  changeaudio,
}
