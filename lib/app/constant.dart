import 'package:flutter/material.dart';
import 'package:flutter_inset_box_shadow/flutter_inset_box_shadow.dart' as inset;

// int maxItemPerPage = 14;

const kPrimaryColor = Color(0xFFf49e9c);
const kPrimaryColorAccent = Color(0xFFcf8685);
const kPrimaryColorAccent2 = Color(0xFFffb6b3);
const kSecondaryColor = Color(0xFFC6807F);

const kBgBlack = Color(0xFF484443);
const kBlackAccent = Color(0xFF3d3a39);
const kBlackAccent2 = Color(0xFF534e4d);
const kBlackAccent3 = Color(0xFF32302f);
const kBlackAccent4 = Color(0xFF5e5857);

const kBlackGradien = Color(0xFF413d3c);
const kBlackGradien2 = Color(0xFF4d4948);

const kDefaultBackgroundDecoration = BoxDecoration(
  gradient: LinearGradient(
    tileMode: TileMode.mirror,
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      kBgBlack,
      kBlackAccent2,
    ],
  ),
);

const kLinearGradient = LinearGradient(
  colors: [
    kPrimaryColorAccent,
    kPrimaryColorAccent2,
  ],
);

const kLinearGradientBlack = LinearGradient(
  tileMode: TileMode.mirror,
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [
    kBgBlack,
    kBlackAccent2,
  ],
);

const kDefBoxShadow = [
  BoxShadow(
    color: kBlackAccent,
    offset: const Offset(4.5, 4.5),
    blurRadius: 6.5,
    spreadRadius: 0.2,
  ),
  BoxShadow(
    color: kBlackAccent2,
    offset: const Offset(-4.5, -4.5),
    blurRadius: 6.5,
    spreadRadius: 0.2,
  ),
];

const kDefBoxShadowPrimaryColor = [
  BoxShadow(
    color: kPrimaryColorAccent,
    offset: const Offset(4.5, 4.5),
    blurRadius: 6.5,
    spreadRadius: 0.2,
  ),
  BoxShadow(
    color: kPrimaryColorAccent2,
    offset: const Offset(-4.5, -4.5),
    blurRadius: 6.5,
    spreadRadius: 0.2,
  ),
];

const kDefBoxShadowInset = [
  inset.BoxShadow(
    color: kBlackAccent3,
    offset: Offset(4, 4),
    blurRadius: 4.5,
    spreadRadius: 0.2,
    inset: true,
  ),
  inset.BoxShadow(
    color: kBlackAccent4,
    offset: Offset(-4, -4),
    blurRadius: 4.5,
    spreadRadius: 0.2,
    inset: true,
  ),
];

const kBgWhite = Color(0xFFfafafa);
const kGrey = Color(0xFF595959);
const kInactiveColor = Color(0xFFa6a6a6);
const kDefaultPicture = 'assets/images/default_picture.jpeg';
const kDefaultFastDuration = Duration(milliseconds: 150);
const kDefaultDuration = Duration(milliseconds: 500);
const kDefaultCurve = Curves.easeInOutCirc;

const kDeck = 'deck';
const kAudioDevice = 'audiodevice';
const kImages = 'images';

TextStyle get kDefaultTextStyle {
  return TextStyle(
    color: kBgBlack,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
  );
}

// const kColor1 = Color(0xff1DAF5E);
// const kColor2 = Color(0xff1A9F56);

// const kColor1 = Color(0xff1F9B75);
// const kColor2 = Color(0xff25B98D);

// const kColor1 = Color(0xff364770);
// const kColor2 = Color(0xff526cab);

// const kColor1 = Colors.lightGreenAccent;
// const kColor2 = Colors.lightGreen;

// const kColor1 = Color(0xFFf49e9c);
// const kColor2 = Color(0xFF484443);



// const kColor3 = Color(0xff20BF66);
