import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cron/cron.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:streamdeck/app/modules/home/controllers/home_controller.dart';

import '../constant.dart';
import '../helpers/decktype.dart';
import '../helpers/functionality.dart';
import '../models/deck_item_model.dart';
import '../modules/settings/controllers/settings_controller.dart';
import '../routes/app_pages.dart';
import '../utils/function_utils.dart';

class SocketController extends GetxController {
  var box = GetStorage();
  // var ip = '10.0.2.2';
  // var ip = '192.168.0.2';
  var connected = false.obs;
  // var port = '6969';
  late Socket socket;
  var cron = Cron();

  var maxItemPerPage = 0.obs;

  @override
  void onInit() {
    super.onInit();
    maxItemPerPage.value = 10;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void arrangeItem(List rawData, {bool resetPageController = false}) {
    late HomeController homeC;
    bool isRegistered = Get.isRegistered<HomeController>();
    if (isRegistered) {
      homeC = Get.find<HomeController>();
    } else {
      homeC = Get.put(HomeController());
    }
    // homeC.pageController.jumpToPage(0);
    homeC.pagePosition.value = 0;
    homeC.listPage.clear();
    homeC.listScenes.assignAll(rawData);
    var tempItem = [];
    for (var i = 0; i < rawData.length; i++) {
      var _item = DeckItemModel(
        msg: Functionality.getScenes.name,
        data: rawData[i],
        type: Decktype.scene.name,
      ).toJson();
      tempItem.add(_item);
    }
    List _resBox = box.read(kDeck) ?? [];
    List temp = [];
    temp.insertAll(0, tempItem);
    temp.addAll(_resBox);
    var loopLength = temp.length / maxItemPerPage.value;
    for (var i = 0; i < loopLength.ceil(); i++) {
      var indexCeil = (i + 1) * maxItemPerPage.value;
      List _temp = [];
      for (var x = indexCeil - (maxItemPerPage.value - 1); x <= indexCeil; x++) {
        if (x > temp.length) {
          break;
        }
        _temp.add(temp[x - 1]);
      }
      logKey('_temp', _temp);
      homeC.listPage.add(_temp);
    }
    homeC.progess.value = 1 / homeC.listPage.length;
  }

  Future<void> connectSocket({required String ip, required String port}) async {
    try {
      if (connected.isFalse) {
        dialogLoading();
        socket = await Socket.connect(ip, int.parse(port), timeout: Duration(seconds: 2));
      }
      logKey('socket', socket.address);
      connected.value = true;
      Get.back();
      logKey('connected');
      Get.offNamed(Routes.HOME);
      socket.listen((event) {
        String rawData = String.fromCharCodes(event);
        var data = json.decode(rawData);
        logKey('msg', data['msg']);
        //* getScenes
        if (data['msg'] == Functionality.getScenes.name) {
          bool isRegistered = Get.isRegistered<HomeController>();
          late HomeController homeC;
          if (isRegistered) {
            homeC = Get.find<HomeController>();
          } else {
            homeC = Get.put(HomeController());
          }
          homeC.listPage.clear();
          getCurrentScene();
          var decode = json.decode(data['scenes']);
          box.write('raw', decode);
          //*
          arrangeItem(decode);
          //*
          // homeC.listScenes.assignAll(decode);
          // var tempItem = [];
          // for (var i = 0; i < decode.length; i++) {
          //   var _item = DeckItemModel(
          //     msg: Functionality.getScenes.name,
          //     data: decode[i],
          //     type: Decktype.scene.name,
          //   ).toJson();
          //   tempItem.add(_item);
          // }
          // List _resBox = box.read(kDeck) ?? [];
          // List temp = [];
          // temp.insertAll(0, tempItem);
          // temp.addAll(_resBox);
          // var loopLength = temp.length / maxItemPerPage;
          // for (var i = 0; i < loopLength.ceil(); i++) {
          //   var indexCeil = (i + 1) * maxItemPerPage;
          //   List _temp = [];
          //   for (var x = indexCeil - (maxItemPerPage - 1); x <= indexCeil; x++) {
          //     if (x > temp.length) {
          //       break;
          //     }
          //     _temp.add(temp[x - 1]);
          //   }
          //   logKey('_temp', _temp);
          //   homeC.listPage.add(_temp);
          // }
          // homeC.progess.value = 1 / homeC.listPage.length;

          // logKey('insert scene', homeC.listPage);
          return;
        }
        //* get currentScene
        if (data['msg'] == Functionality.getCurrentScene.name) {
          bool isRegistered = Get.isRegistered<HomeController>();
          late HomeController homeC;
          if (isRegistered) {
            homeC = Get.find<HomeController>();
          } else {
            homeC = Get.put(HomeController());
          }
          // logKey('current_scene_name', data['current_scene_name']);
          homeC.currentSceneName.value = data['current_scene_name'];
        }
        //* switch scene
        if (data['msg'] == Functionality.switchScene.name) {
          bool isRegistered = Get.isRegistered<HomeController>();
          late HomeController homeC;
          if (isRegistered) {
            homeC = Get.find<HomeController>();
          } else {
            homeC = Get.put(HomeController());
          }
          homeC.currentSceneName.value = data['scene_name'];
        }
        //*get  filename
        if (data['msg'] == Functionality.filename.name) {
          // logKey('masuk filename');
          // var decode = json.decode(decoded['filename']);
        }
        //* get audiodevices
        if (data['msg'] == Functionality.audiodevices.name) {
          var settingsC = Get.find<SettingsController>();
          var res = data['list_audio_devices'];
          List<String> a = [];
          for (String element in res) {
            a.add(element);
          }
          settingsC.listAudioDevices.clear();
          settingsC.listAudioDevices.addAll(a);
          settingsC.listAudioDevices.add('Default');
        }
        //* change audiodevice
        if (data['msg'] == Functionality.changeaudio.name) {}
      }, onDone: () {
        bool isRegistered = Get.isRegistered<HomeController>();
        late HomeController homeC;
        if (isRegistered) {
          homeC = Get.find<HomeController>();
        } else {
          homeC = Get.put(HomeController());
        }
        homeC.listPage.clear();
        logKey('onDone', homeC.listPage.length);
        // logKey('socket listen onDone');
        // networkDialog(message: "You've been disconnected");
        Get.offAllNamed(
          Routes.CONNECT_SERVER,
          arguments: {
            'disconnected': true,
          },
        );
        connected.value = false;
      }, onError: (error, StackTrace trace) {
        connected.value = false;
        networkDialog(message: 'Connection error ${error.toString()}');
        ;
        logKey('socket listen onError');
      });
      socket.write("st_start");
    } catch (e) {
      Get.back();
      networkDialog(message: 'Error connect to server');
      logKey('error connect socket', e.toString());
    }
  }

  Future<void> disconnectSocket() async {
    try {
      socket.close();
    } catch (e) {
      logKey('error Disconnect socket', e.toString());
    }
  }

  void hideSource({required String sourceName}) {
    socket.write('s_hide$sourceName');
  }

  void switchScene({required String sceneName}) {
    socket.write('scene$sceneName');
  }

  void changeAudioDevice(String audioDeviceName) {
    logKey('changeAudioDevice', audioDeviceName);
    socket.write('changeaudio$audioDeviceName');
  }

  void playSound(String path, {String? audioDevice}) {
    path = path.replaceAll(r'\', r'\\');
    var audioDeviceName = 'Speakers (Realtek High Definition Audio)';

    socket.write('playsound$path|$audioDeviceName');
  }

  void filename() {
    socket.write('filenameD:/');
  }

  void getScenes() {
    socket.write('getScenes');
  }

  void getCurrentScene() {
    socket.write('getCurrentScene');
  }

  late Timer time;

  void cronScene() {
    // time = Timer.periodic(
    //   Duration(seconds: 5),
    //   (timer) {
    //     getScenes();
    //   },
    // );
    // try {
    //   cron.schedule(
    //     // Schedule(
    //     //   seconds: 5,
    //     // ),
    //     Schedule.parse(''),
    //     () async {
    //       logKey('cron jalan');
    //       getScenes();
    //     },
    //   );
    // } on ScheduleParseException catch (e) {
    //   logKey('error croScene', e.toString());
    // }
  }
}
