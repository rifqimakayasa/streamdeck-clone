class DeckItemModel {
  DeckItemModel({
    required this.msg,
    required this.data,
    required this.type,
    // this.type = '',
    this.detail = '',
    this.image_uuid = '',
    // this.path = '',
  });
  String msg;
  String data;
  String type;
  String detail;
  // String path;
  String image_uuid;

  factory DeckItemModel.fromJson(Map<String, dynamic> json) => DeckItemModel(
        msg: json['msg'],
        data: json['data'],
        type: json['type'],
        detail: json['detail'],
        image_uuid: json['image_uuid'],
        // path: json['path'],
      );

  toJson() => {
        'msg': msg,
        'data': data,
        'type': type,
        'detail': detail,
        'image_uuid': image_uuid,
        // 'path': path,
      };
}
