import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:streamdeck/app/config/global_binding.dart';
import 'package:streamdeck/app/constant.dart';

import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage().initStorage;
  var box = GetStorage();
  var _audioDevice = box.read(kAudioDevice) ?? '';
  if (_audioDevice == '') {
    box.write(kAudioDevice, 'Default');
  }
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]);
  runApp(
    GetMaterialApp(
      title: "Application",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      initialBinding: GlobalBinding(),
      theme: ThemeData(
        textTheme: GoogleFonts.workSansTextTheme(),
        primaryColor: kPrimaryColor,
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: kPrimaryColor,
          secondary: kPrimaryColorAccent,
        ),
        appBarTheme: AppBarTheme(
          color: kBgBlack,
        ),
      ),
    ),
  );
}
